$(".bg-menu").click(function () {
  if ($(this).hasClass("active")) {
    $(this).removeClass("active");
    $("header .list-page").removeClass("active");
    $("header ").removeClass("active");
  } else {
    $(this).addClass("active");
    $("header .list-page").addClass("active");
    $("header").addClass("active");
  }
});

$(".language").click(function () {
  if ($(".language .list-lang").hasClass("active")) {
    $(".language .list-lang").removeClass("active");
    $("header ").removeClass("active");
  } else {
    $(".language .list-lang").addClass("active");
    $("header").addClass("active");
  }
});

$(".list-lang li").click(function () {
  const lang = $(this).data("lang");
  $(".option").html(document.getElementById(lang).innerHTML);
  $("#langCode").val(lang);
  setTimeout(translate(), 1000);
});

function translate() {
  var langCode = $("#langCode").val() || "en";
  $.getJSON("../lang/" + langCode + ".json", function (data) {
    $("[langKey]").each(function (index) {
      var strTr = data[$(this).attr("langKey")];
      $(this).html(strTr);
    });
  }).fail(function () {
    console.log("An error has occurred.");
  });
}
$(window).scroll(function () {
  if ($(window).scrollTop() >= 10) {
    $(".main").addClass("main-ani");
  }
});
// var a = 0;
// $(window).scroll(function () {
//   var oTop = $("#counter-box").offset().top - window.innerHeight;
//   if (a == 0 && $(window).scrollTop() > oTop) {
//     $(".counter").each(function () {
//       var $this = $(this),
//         countTo = $this.attr("data-number");
//       $({
//         countNum: $this.text(),
//       }).animate(
//         {
//           countNum: countTo,
//         },

//         {
//           duration: 3000,
//           easing: "swing",
//           step: function () {
//             //$this.text(Math.ceil(this.countNum));
//             $this.text(Math.ceil(this.countNum).toLocaleString("en"));
//           },
//           complete: function () {
//             $this.text(Math.ceil(this.countNum).toLocaleString("en"));
//             //alert('finished');
//           },
//         }
//       );
//     });
//     a = 1;
//   }
// });
var isAlreadyRun = false;
$(window).scroll(function() {

    $('.counter-show').each(function(i) {
        var bottom_of_object = $(this).position().top + $(this).outerHeight() / 2;
        var bottom_of_window = $(window).scrollTop() + $(window).height();

        if (bottom_of_window > (bottom_of_object + 20)) {
            if (!isAlreadyRun) {
                $('.count-number').each(function() {

                    $(this).prop('Counter', 0).animate({
                        Counter: $(this).text()
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function(now) {
                            $(this).text(Math.ceil(now));
                        }
                    });
                });
            }
            isAlreadyRun = true;
        }
    });
});
$(".banner-list").slick({
  slidesToShow: 1,
  dots: true,
});
$(".tiktok-list").slick({
  centerMode: true,
  centerPadding: "160px",
  slidesToShow: 1,
  dots: true,
  prevArrow: "<img class='slick-arrow slick-prev' src='./images/left.svg'>",
  nextArrow: "<img class='slick-arrow slick-next' src='./images/right.svg'>",
  responsive: [
    {
      breakpoint: 1280,
      settings: {
        centerMode: true,
        centerPadding: "90px",
      },
    },
    {
      breakpoint: 480,
      settings: {
        centerMode: true,
        centerPadding: "80px",
      },
    },
  ],
});
$(window).scroll(function () {
  if ($(window).scrollTop() >= 10) {
    $(".header").addClass("header-bg");
  } else {
    $(".header").removeClass("header-bg");
  }
});
AOS.init({
  offset: 100, // offset (in px) from the original trigger point
  delay: 100, // values from 0 to 3000, with step 50ms
  duration: 700, // values from 0 to 3000, with step 50ms
  // disable: 'mobile'

});
$(".mobile-news").slick({
  slidesToShow: 1,
  dots: true,
});

$(window).on("load", function () {
  $(".loader-wrapper").fadeOut("slow");
});

$(document).ready(function () {
  var $videoSrc;
  $(".play").click(function () {
    $videoSrc = $(this).data("src");
  });

  $(".modal-tiktok").on("shown.bs.modal", function (e) {
    $(".frame_video").attr(
      "src",
        $videoSrc +
        "?lang=vi&referrer=http%3A%2F%2Flocalhost%3A3000%2F"
    );
  });
  $(".modal-tiktok").on("hide.bs.modal", function (e) {
    $(".frame_video").attr("src", "");
  });
});
